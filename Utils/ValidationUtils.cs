﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ArchetypeEditor.Utils
{
    public static class ValidationUtils
    {
        public static bool ValidateInt(int val, int minVal, int maxVal)
        {
            return val >= minVal && val <= maxVal;
        }
        public static bool ValidateText(string text, int maxLen)
        {
            return !string.IsNullOrWhiteSpace(text) && text.Length <= maxLen;
        }
        
        public static bool ValidateEmail(string email)
        {
            Regex validateEmailRegex = new Regex("^\\S+@\\S+\\.\\S+$");
            return !string.IsNullOrWhiteSpace(email) && ValidateText(email, 128) && validateEmailRegex.IsMatch(email);
        }

        public static bool ValidatePIN(string pin)
        {
            Regex validatePINRegex = new Regex("^[0-9]+$");

            if(!string.IsNullOrWhiteSpace(pin) && validatePINRegex.IsMatch(pin))
            {
                List<int> nums = new List<int>();
                foreach (char ch in pin)
                {
                    try
                    {
                        if (int.TryParse(ch.ToString(), out int num))
                        {
                            nums.Add(num);
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch
                    {
                        break;
                    }
                }
                if(nums.Count == 10)
                {
                    int oddSum = 0;
                    int evenSum = 0;
                    for(int i = 0; i < nums.Count; i++)
                    {
                        if (i % 2 == 0)
                        {
                            evenSum += nums[i];
                        } else
                        {
                            oddSum += nums[i];
                        }
                    }
                    return (oddSum - evenSum) % 11 == 0 ? true : false;
                } else
                {
                    return false;
                }
            } else
            {
                return false;
            }  
        }

        public static bool ValidatePhone(string phone)
        {
            Regex validatePhoneRegex = new Regex("^[0-9]+$");
            return !string.IsNullOrWhiteSpace(phone) && ValidateText(phone, 16) && validatePhoneRegex.IsMatch(phone);
        }
    }
}
