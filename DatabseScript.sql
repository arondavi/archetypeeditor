CREATE DATABASE `archetypedb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `bloodpressure` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `systolic` int NOT NULL,
  `diastolic` int NOT NULL,
  `meanap` int NOT NULL,
  `pulse` int NOT NULL,
  `clinicalinter` varchar(200) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `visitID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_bloodpressure_visit_idx` (`visitID`),
  CONSTRAINT `fk_bloodpressure_visit` FOREIGN KEY (`visitID`) REFERENCES `visit` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `healtheducation` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `topicName` varchar(128) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `clinicalInd` varchar(64) NOT NULL,
  `method` varchar(64) NOT NULL,
  `outcome` varchar(300) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `visitID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_heatheducation_visit_idx` (`visitID`),
  CONSTRAINT `fk_heatheducation_visit` FOREIGN KEY (`visitID`) REFERENCES `visit` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `healthriskassessment` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `healthRisk` varchar(300) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `visitID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_healthriskassessment_visit_idx` (`visitID`),
  CONSTRAINT `fk_healthriskassessment_visit` FOREIGN KEY (`visitID`) REFERENCES `visit` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `medicationmanagement` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `medicationItem` varchar(128) NOT NULL,
  `amount` varchar(32) NOT NULL,
  `clinicalInd` varchar(64) NOT NULL,
  `substitution` int NOT NULL,
  `substitutionReason` varchar(256) NOT NULL,
  `route` varchar(64) NOT NULL,
  `bodySite` varchar(64) NOT NULL,
  `patientGuidance` varchar(256) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `visitID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_medicationmanagement_visit_idx` (`visitID`),
  CONSTRAINT `fk_medicationmanagement_visit` FOREIGN KEY (`visitID`) REFERENCES `visit` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `medicationorder` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `medicationItem` varchar(128) NOT NULL,
  `route` varchar(256) NOT NULL,
  `bodySite` varchar(64) NOT NULL,
  `odd` varchar(500) DEFAULT NULL,
  `patientInfo` varchar(500) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `visitID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_medicationorder_visit_idx` (`visitID`),
  CONSTRAINT `fk_medicationorder_visit` FOREIGN KEY (`visitID`) REFERENCES `visit` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `patient` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(64) NOT NULL,
  `lastName` varchar(64) NOT NULL,
  `persIdenNum` varchar(10) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `riskfactor` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `riskFactor` varchar(300) NOT NULL,
  `presence` int NOT NULL,
  `description` varchar(500) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `healthRiskAssessmentID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_riskfactor_healthriskassessment_idx` (`healthRiskAssessmentID`),
  CONSTRAINT `fk_riskfactor_healthriskassessment` FOREIGN KEY (`healthRiskAssessmentID`) REFERENCES `healthriskassessment` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `visit` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `dateTime` datetime NOT NULL,
  `patientID` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_patient_visit_idx` (`patientID`),
  CONSTRAINT `fk_patient_visit` FOREIGN KEY (`patientID`) REFERENCES `patient` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
