﻿using ArchetypeEditor.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ArchetypeEditor.Converters
{
    public class ArchetypeTypeToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is BloodPressure)
                return "heart";
            else if (value is MedicationOrder)
                return "medkit";
            else if (value is HealthEducation)
                return "book";
            else if (value is HealthRiskAssessment)
                return "bullhorn";
            else if (value is MedicationManagement)
                return "pencil";
            else
                return "file";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
