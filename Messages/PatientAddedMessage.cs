﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class PatientAddedMessage : ValueChangedMessage<Patient>
    {
        public PatientAddedMessage(Patient p) : base(p)
        {
        }
    }
}
