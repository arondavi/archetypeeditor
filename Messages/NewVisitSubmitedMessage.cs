﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;
using System.Collections.Generic;

namespace ArchetypeEditor.Messages
{
    public class NewVisitSubmitedMessage : ValueChangedMessage<List<Archetype>>
    {
        public NewVisitSubmitedMessage(List<Archetype> archs) : base(archs)
        {
        }
    }
}
