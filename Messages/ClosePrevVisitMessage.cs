﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchetypeEditor.Messages
{
    public class ClosePrevVisitMessage : ValueChangedMessage<string>
    {
        public ClosePrevVisitMessage(string s) : base(s)
        {
        }
    }
}
