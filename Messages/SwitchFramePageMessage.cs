﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class SwitchFramePageMessage : ValueChangedMessage<string>
    {
        public SwitchFramePageMessage(string page) : base(page)
        {
        }
    }
}
