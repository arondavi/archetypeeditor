﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class PatientSelectedMessage : ValueChangedMessage<Patient>
    {
        public PatientSelectedMessage(Patient p) : base(p)
        {
        }
    }
}
