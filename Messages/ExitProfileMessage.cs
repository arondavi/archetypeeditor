﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class ExitProfileMessage : ValueChangedMessage<Patient>
    {
        public ExitProfileMessage(Patient p) : base(p)
        {
        }
    }
}
