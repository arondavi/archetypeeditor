﻿using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class BackToNewVisitMessage : ValueChangedMessage<string>
    {
        public BackToNewVisitMessage(string s) : base(s)
        {
        }
    }
}
