﻿using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.Messaging.Messages;

namespace ArchetypeEditor.Messages
{
    public class NewArchetypeAddedMessage : ValueChangedMessage<Archetype>
    {
        public NewArchetypeAddedMessage(Archetype a) : base(a)
        {
        }
    }
}
