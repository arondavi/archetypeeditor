﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchetypeEditor.Messages
{
    public class ShowPrevVisitMessage : ValueChangedMessage<int>
    {
        public ShowPrevVisitMessage(int visitID) : base(visitID)
        {
        }
    }
}
