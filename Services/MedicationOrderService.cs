﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class MedicationOrderService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static void CreateMO(MedicationOrder mo)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.medicationorder(medicationItem, route, bodySite, odd, patientInfo, comment, visitID) values (@medItem, @route, @boS, @odd, @patInfo, @com, @vID)"
                };
                command.Parameters.AddWithValue("@medItem", mo.MedicationItem);
                command.Parameters.AddWithValue("@route", mo.Route);
                command.Parameters.AddWithValue("@boS", mo.BodySite);
                command.Parameters.AddWithValue("@odd", mo.Odd);
                command.Parameters.AddWithValue("@patInfo", mo.PatientInfo);
                command.Parameters.AddWithValue("@com", mo.Comment);
                command.Parameters.AddWithValue("@vID", mo.VisitID);

                command.ExecuteReader();
            }
        }

        public static List<MedicationOrder> GetMOByVisitID(int visitID)
        {
            List<MedicationOrder> mos = new List<MedicationOrder>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select medicationItem, route, bodySite, odd, patientInfo, comment from archetypedb.medicationorder where archetypedb.medicationorder.visitID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string mi = rdr.IsDBNull(0) ? "" : rdr.GetString(0);
                    string ro = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    string bs = rdr.IsDBNull(2) ? "" : rdr.GetString(2);
                    string odd = rdr.IsDBNull(3) ? "" : rdr.GetString(3);
                    string pi = rdr.IsDBNull(4) ? "" : rdr.GetString(4);
                    string comm = rdr.IsDBNull(5) ? "" : rdr.GetString(5);
                    mos.Add(new MedicationOrder(mi, ro, bs, odd, pi, comm));
                }
            }
            return mos;
        }
    }
}
