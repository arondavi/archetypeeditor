﻿using ArchetypeEditor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchetypeEditor.Services
{
    public static class ArchetypeService
    {
        public static List<Archetype> GetArchetypesByVisitID(int visitID)
        {
            List<Archetype> arch = new List<Archetype>();
            foreach (var x in BloodPressureService.GetBPByVisitID(visitID))
            {
                arch.Add(x);
            }
            foreach (var x in HealthEducationService.GetHEByVisitID(visitID))
            {
                arch.Add(x);
            }
            foreach (var x in HealthRiskAssessmentService.GetHRAByVisitID(visitID))
            {
                arch.Add(x);
            }
            foreach (var x in MedicationManagementService.GetMMByVisitID(visitID))
            {
                arch.Add(x);
            }
            foreach (var x in MedicationOrderService.GetMOByVisitID(visitID))
            {
                arch.Add(x);
            }

            return arch;
        }
    }
}
