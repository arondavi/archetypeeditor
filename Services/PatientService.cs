﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class PatientService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static List<Patient> GetAllPatients()
        {
            List<Patient> patients = new List<Patient>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select * from archetypedb.patient"
                };
                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string fn = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    string ln = rdr.IsDBNull(2) ? "" : rdr.GetString(2);
                    string pin = rdr.IsDBNull(3) ? "" : rdr.GetString(3);
                    string em = rdr.IsDBNull(4) ? "" : rdr.GetString(4);
                    string ph = rdr.IsDBNull(5) ? "" : rdr.GetString(5);
                    patients.Add(new Patient(rdr.GetInt32(0), fn, ln, pin, em, ph));
                }
            }
            return patients;
        }

        public static void CreatePatient(Patient patient)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.patient(firstName, lastName, persIdenNum, email, phone) values (@fn, @ln, @pin, @email, @phone)"
                };
                command.Parameters.AddWithValue("@fn", patient.FirstName);
                command.Parameters.AddWithValue("@ln", patient.LastName);
                command.Parameters.AddWithValue("@pin", patient.PersIdenNum);
                command.Parameters.AddWithValue("@email", patient.Email);
                command.Parameters.AddWithValue("@phone", patient.Phone);

                command.ExecuteReader();
            }
        }
    }
}
