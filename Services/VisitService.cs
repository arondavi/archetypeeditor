﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class VisitService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static List<Visit> GetAllVisits()
        {
            List<Visit> visits = new List<Visit>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select ID, dateTime from archetypedb.visit"
                };
                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string dt = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    visits.Add(new Visit(rdr.GetInt32(0), DateTime.Parse(dt)));
                }
            }
            return visits;
        }

        public static List<Visit> GetPrevVisitsByPatientID(int id)
        {
            List<Visit> visits = new List<Visit>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select ID, dateTime from archetypedb.visit v where v.patientID = @id"
                };
                command.Parameters.AddWithValue("@id", id);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    visits.Add(new Visit(rdr.GetInt32(0), DateTime.Parse(rdr.GetString(1))));
                }
            }
            return visits;
        }

        public static Visit CreateVisit(Visit visit)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.visit(dateTime, patientID) values (@created, @patientID); SELECT LAST_INSERT_ID();"
                };
                command.Parameters.AddWithValue("@created", visit.Created.ToString("yyyy-MM-dd HH:mm:ss"));
                command.Parameters.AddWithValue("@patientID", visit.Patient.ID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    visit.ID = rdr.GetInt32(0);
                    return visit;
                }
            }
            return null;
        }

        public static Visit GetVisitByID(int visitID)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select * from archetypedb.visit where archetypedb.visit.ID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    return new Visit(rdr.GetInt32(0), DateTime.Parse(rdr.GetString(1)));
                }
            }
            return null;
        }
    }
}
