﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class HealthRiskAssessmentService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static void CreateHRA(HealthRiskAssessment hra)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.healthriskassessment(healthRisk, comment, visitID) values (@healthRisk, @com, @vID); SELECT LAST_INSERT_ID();"
                };
                command.Parameters.AddWithValue("@healthRisk", hra.HealthRisk);
                command.Parameters.AddWithValue("@com", hra.Comment);
                command.Parameters.AddWithValue("@vID", hra.VisitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    hra.ID = rdr.GetInt32(0);
                }
                rdr.Close();

                foreach (RiskFactor rf in hra.RiskFactors)
                {
                    MySqlCommand comm = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "insert into archetypedb.riskfactor(riskFactor, presence, description, comment, healthRiskAssessmentID) values (@rf, @pres, @des, @com, @hraID)"
                    };
                    comm.Parameters.AddWithValue("@rf", rf.RiskFactorN);
                    comm.Parameters.AddWithValue("@pres", rf.Presence);
                    comm.Parameters.AddWithValue("@des", rf.Description);
                    comm.Parameters.AddWithValue("@com", rf.Comment);
                    comm.Parameters.AddWithValue("@hraID", hra.ID);

                    comm.ExecuteReader().Close();
                }
            }
        }

        public static List<HealthRiskAssessment> GetHRAByVisitID(int visitID)
        {
            List<HealthRiskAssessment> hras = new List<HealthRiskAssessment>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select ID, healthRisk, comment from archetypedb.healthriskassessment where archetypedb.healthriskassessment.visitID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string hr = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    string comm = rdr.IsDBNull(2) ? "" : rdr.GetString(2);
                    hras.Add(new HealthRiskAssessment(rdr.GetInt32(0), hr, comm));
                }
                rdr.Close();
                
                foreach (var hra in hras)
                {
                    List<RiskFactor> rfs = new List<RiskFactor>();
                    MySqlCommand cmd = new MySqlCommand
                    {
                        Connection = connection,
                        CommandText = "select riskFactor, presence, description, comment from archetypedb.riskfactor where archetypedb.riskfactor.healthRiskAssessmentID=@hraID"
                    };
                    cmd.Parameters.AddWithValue("@hraID", hra.ID);

                    using MySqlDataReader rdr2 = cmd.ExecuteReader();
                    while (rdr2.Read())
                    {
                        string rf = rdr2.IsDBNull(0) ? "" : rdr2.GetString(0);
                        string des = rdr2.IsDBNull(2) ? "" : rdr2.GetString(2);
                        string comm = rdr2.IsDBNull(3) ? "" : rdr2.GetString(3);
                        rfs.Add(new RiskFactor(rf, rdr2.GetInt32(1), des, comm));
                    }
                    hra.RiskFactors = rfs;
                }
            }
            return hras;
        }
    }
}
