﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class MedicationManagementService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static void CreateMM(MedicationManagement mm)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.medicationmanagement(medicationItem, amount, clinicalInd, substitution, substitutionReason, route, bodySite, patientGuidance, comment, visitID) values (@medItem, @amount, @cliInd, @sub, @subR, @route, @bS, @patGui, @com, @vID)"
                };
                command.Parameters.AddWithValue("@medItem", mm.MedicationItem);
                command.Parameters.AddWithValue("@amount", mm.Amount);
                command.Parameters.AddWithValue("@cliInd", mm.ClinicalInd);
                command.Parameters.AddWithValue("@sub", mm.Substitution);
                command.Parameters.AddWithValue("@subR", mm.SubstitutionReason);
                command.Parameters.AddWithValue("@route", mm.Route);
                command.Parameters.AddWithValue("@bS", mm.BodySite);
                command.Parameters.AddWithValue("@patGui", mm.PatientGuidance);
                command.Parameters.AddWithValue("@com", mm.Comment);
                command.Parameters.AddWithValue("@vID", mm.VisitID);

                command.ExecuteReader();
            }
        }

        public static List<MedicationManagement> GetMMByVisitID(int visitID)
        {
            List<MedicationManagement> mms = new List<MedicationManagement>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select medicationItem, amount, clinicalInd, substitution, substitutionReason, route, bodySite, patientGuidance, comment from archetypedb.medicationmanagement where archetypedb.medicationmanagement.visitID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string mi = rdr.IsDBNull(0) ? "" : rdr.GetString(0);
                    string am = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    string ci = rdr.IsDBNull(2) ? "" : rdr.GetString(2);
                    string sr = rdr.IsDBNull(4) ? "" : rdr.GetString(4);
                    string ro = rdr.IsDBNull(5) ? "" : rdr.GetString(5);
                    string bs = rdr.IsDBNull(6) ? "" : rdr.GetString(6);
                    string pg = rdr.IsDBNull(7) ? "" : rdr.GetString(7);
                    string comm = rdr.IsDBNull(8) ? "" : rdr.GetString(8);
                    mms.Add(new MedicationManagement(mi, am, ci, rdr.GetInt32(3), sr, ro, bs, pg, comm));
                }
            }
            return mms;
        }
    }
}
