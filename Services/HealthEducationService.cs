﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class HealthEducationService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static void CreateHE(HealthEducation he)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.healtheducation(topicName, description, clinicalInd, method, outcome, comment, visitID) values (@topicN, @des, @cliInd, @method, @out, @com, @vID)"
                };
                command.Parameters.AddWithValue("@topicN", he.TopicName);
                command.Parameters.AddWithValue("@des", he.Description);
                command.Parameters.AddWithValue("@cliInd", he.ClinicalInd);
                command.Parameters.AddWithValue("@method", he.Method);
                command.Parameters.AddWithValue("@out", he.Outcome);
                command.Parameters.AddWithValue("@com", he.Comment);
                command.Parameters.AddWithValue("@vID", he.VisitID);

                command.ExecuteReader();
            }
        }

        public static List<HealthEducation> GetHEByVisitID(int visitID)
        {
            List<HealthEducation> hes = new List<HealthEducation>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select topicName, description, clinicalInd, method, outcome, comment from archetypedb.healtheducation where archetypedb.healtheducation.visitID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string topic = rdr.IsDBNull(0) ? "" : rdr.GetString(0);
                    string des = rdr.IsDBNull(1) ? "" : rdr.GetString(1);
                    string ci = rdr.IsDBNull(2) ? "" : rdr.GetString(2);
                    string met = rdr.IsDBNull(3) ? "" : rdr.GetString(3);
                    string outc = rdr.IsDBNull(4) ? "" : rdr.GetString(4);
                    string comm = rdr.IsDBNull(5) ? "" : rdr.GetString(5);
                    hes.Add(new HealthEducation(topic, des, ci, met, outc, comm));
                }
            }
            return hes;
        }
    }
}
