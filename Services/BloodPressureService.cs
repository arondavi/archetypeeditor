﻿using ArchetypeEditor.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace ArchetypeEditor.Services
{
    public static class BloodPressureService
    {
        private static string cs = ConfigurationManager.ConnectionStrings["archetypedb"].ConnectionString;

        public static void CreateBP(BloodPressure bp)
        {
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "insert into archetypedb.bloodpressure(systolic, diastolic, meanap, pulse, clinicalinter, comment, visitID) values (@sys, @dia, @mean, @p, @clin, @com, @vID)"
                };
                command.Parameters.AddWithValue("@sys", bp.Systolic);
                command.Parameters.AddWithValue("@dia", bp.Diastolic);
                command.Parameters.AddWithValue("@mean", bp.MeanAP);
                command.Parameters.AddWithValue("@p", bp.Pulse);
                command.Parameters.AddWithValue("@clin", bp.ClinicalInter);
                command.Parameters.AddWithValue("@com", bp.Comment);
                command.Parameters.AddWithValue("@vID", bp.VisitID);

                command.ExecuteReader();
            }
        }

        public static List<BloodPressure> GetBPByVisitID(int visitID)
        {
            List<BloodPressure> bps = new List<BloodPressure>();
            using (MySqlConnection connection = new MySqlConnection(cs))
            {
                connection.Open();

                MySqlCommand command = new MySqlCommand
                {
                    Connection = connection,
                    CommandText = "select systolic, diastolic, meanap, pulse, clinicalinter, comment from archetypedb.bloodpressure where archetypedb.bloodpressure.visitID=@vID"
                };
                command.Parameters.AddWithValue("@vID", visitID);

                using MySqlDataReader rdr = command.ExecuteReader();
                while (rdr.Read())
                {
                    string ci = rdr.IsDBNull(4) ? "" : rdr.GetString(4);
                    string comm = rdr.IsDBNull(5) ? "" : rdr.GetString(5);
                    bps.Add(new BloodPressure(rdr.GetInt32(0), rdr.GetInt32(1), rdr.GetInt32(2), rdr.GetInt32(3), ci, comm));
                }
            }
            return bps;
        }
    }
}
