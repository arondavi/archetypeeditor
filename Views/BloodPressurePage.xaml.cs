﻿using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace ArchetypeEditor.Views
{
    /// <summary>
    /// Interakční logika pro BloodPressurePage.xaml
    /// </summary>
    public partial class BloodPressurePage : Page
    {
        public BloodPressurePage()
        {
            InitializeComponent();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
