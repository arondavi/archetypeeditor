﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class HealthRiskAssessmentViewModel : ObservableObject
    {
        private ObservableCollection<RiskFactor> _riskFactors;
        public ObservableCollection<RiskFactor> RiskFactors
        {
            get => _riskFactors;
            set => SetProperty(ref _riskFactors, value);
        }

        public ICommand CreateHRACommand { get; }
        public ICommand BackCommand { get; }
        public ICommand AddRFCommand { get; }
        public ICommand RemoveRFCommand { get; }

        private HealthRiskAssessment _currentHRA;
        public HealthRiskAssessment CurrentHRA
        {
            get => _currentHRA;
            set => SetProperty(ref _currentHRA, value);
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => SetProperty(ref _isEditMode, value);
        }

        private bool _showDetailMode;
        public HealthRiskAssessmentViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = true;
            RiskFactors = new ObservableCollection<RiskFactor>() { new RiskFactor() };
            _currentHRA = new HealthRiskAssessment();
            CreateHRACommand = new RelayCommand(CreateHRA);
            AddRFCommand = new RelayCommand(AddRF);
            RemoveRFCommand = new RelayCommand<RiskFactor>(RemoveRF);
            BackCommand = new RelayCommand(Back);
        }

        public HealthRiskAssessmentViewModel(HealthRiskAssessment hra, bool showDetailMode)
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = false;
            _showDetailMode = showDetailMode;
            RiskFactors = new ObservableCollection<RiskFactor>(hra.RiskFactors);
            _currentHRA = hra;
            CreateHRACommand = new RelayCommand(CreateHRA);
            AddRFCommand = new RelayCommand(AddRF);
            RemoveRFCommand = new RelayCommand<RiskFactor>(RemoveRF);
            BackCommand = new RelayCommand(Back);
        }

        public void CreateHRA()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                CurrentHRA.RiskFactors = RiskFactors.ToList();
                WeakReferenceMessenger.Default.Send(new NewArchetypeAddedMessage(CurrentHRA));
                CurrentHRA = new HealthRiskAssessment();
                RiskFactors = new ObservableCollection<RiskFactor>() { new RiskFactor() };
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void AddRF()
        {
            RiskFactors.Add(new RiskFactor());
        }

        public void RemoveRF(RiskFactor rf)
        {
            RiskFactors.Remove(rf);
        }

        public void Back()
        {
            if (!IsEditMode)
            {
                if (_showDetailMode)
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("Prev"));
                }
                else
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("New"));
                }
            } else
            {
                WeakReferenceMessenger.Default.Send(new BackToNewVisitMessage(""));
            }
            
            CurrentHRA = new HealthRiskAssessment();
            RiskFactors = new ObservableCollection<RiskFactor>() { new RiskFactor() };
            IsEditMode = true;
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateText(_currentHRA.HealthRisk, 300))
            {
                _ = sb.Append("Wrong 'Health risk' format!\n");
            }
            if (_currentHRA.Comment != null && _currentHRA.Comment.Length > 0 && !ValidationUtils.ValidateText(_currentHRA.Comment, 500))
            {
                _ = sb.Append("Wrong 'Comment' format!\n");
            }

            for(int i = 0; i < RiskFactors.Count; i++)
            {
                if (!ValidationUtils.ValidateText(RiskFactors[i].RiskFactorN, 300))
                {
                    _ = sb.Append("Risk factor (" + (i+1) + "): Wrong 'Risk factor' format!\n");
                }
                if (!ValidationUtils.ValidateText(RiskFactors[i].Description, 500))
                {
                    _ = sb.Append("Risk factor (" + (i+1) + "): Wrong 'Description' format!\n");
                }
                if (RiskFactors[i].Comment != null && RiskFactors[i].Comment.Length > 0 && !ValidationUtils.ValidateText(RiskFactors[i].Comment, 500))
                {
                    _ = sb.Append("Risk factor (" + (i+1) + "): Wrong 'Comment' format!\n");
                }
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
