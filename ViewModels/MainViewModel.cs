﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class MainViewModel : ObservableRecipient, IRecipient<PatientSelectedMessage>, IRecipient<PatientAddedMessage>, IRecipient<ExitProfileMessage>, IRecipient<NewVisitSubmitedMessage>, IRecipient<ShowErrorMessage>
    {
        private readonly IDictionary<string, Page> pages;

        public ICommand SwitchFramePageCommand { get; }
        public ICommand CloseMSGBCommand { get; }

        private Page _framePage;

        public Page FramePage
        {
            get => _framePage;
            set => SetProperty(ref _framePage, value);
        }

        private bool _menuVisible;
        public bool MenuVisible
        {
            get => _menuVisible;
            set => SetProperty(ref _menuVisible, value);
        }

        private bool _showMSGB;
        public bool ShowMSGB
        {
            get => _showMSGB;
            set => SetProperty(ref _showMSGB, value);
        }

        private string _messageMSGB;

        public string MessageMSGB
        {
            get => _messageMSGB;
            set => SetProperty(ref _messageMSGB, value);
        }


        public MainViewModel()
        {
            _showMSGB = false;
            _menuVisible = true;
            _messageMSGB = "";
            SwitchFramePageCommand = new RelayCommand<string>(SwitchFramePage);
            CloseMSGBCommand = new RelayCommand(CloseMSGB);

            WeakReferenceMessenger.Default.RegisterAll(this);

            pages = new Dictionary<string, Page>() {
                {"Home", new HomePage()},
                {"Pat", new PatientPage()},
                {"PAT", new PatientProfilePage()}
            };

            SwitchFramePage("Home");
        }

        public void SwitchFramePage(string key)
        {
            MenuVisible = key != "PAT";

            if (pages.TryGetValue(key, out Page page))
            {
                FramePage = page;
            }
        }

        public void OpenMSGB(string message)
        {
            MessageMSGB = message;
            ShowMSGB = true;
        }

        public void CloseMSGB()
        {
            ShowMSGB = false;
            MessageMSGB = "";
        }

        public void Receive(PatientAddedMessage message)
        {
            if (message.Value == null)
            {
                SwitchFramePage("Home");
                return;
            }
            SwitchFramePage("Home");
            OpenMSGB("New patient has been created!");
        }

        public void Receive(PatientSelectedMessage message)
        {
            SwitchFramePage("PAT");
        }

        public void Receive(ExitProfileMessage message)
        {
            SwitchFramePage("Home");
        }

        public void Receive(NewVisitSubmitedMessage message)
        {
            SwitchFramePage("Home");
            OpenMSGB("New visit has been submited!");
        }

        public void Receive(ShowErrorMessage message)
        {
            OpenMSGB(message.Value);
        }
    }
}
