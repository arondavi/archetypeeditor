﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Models.Enums;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class MedicationManagementViewModel : ObservableObject
    {
        public ObservableCollection<SubstitutionType> SubstitutionTypes { get; }

        public ICommand CreateMMCommand { get; }
        public ICommand BackCommand { get; }

        private MedicationManagement _currentMM;

        public MedicationManagement CurrentMM
        {
            get => _currentMM;
            set => SetProperty(ref _currentMM, value);
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => SetProperty(ref _isEditMode, value);
        }

        private bool _showDetailMode;

        public MedicationManagementViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = true;
            _currentMM = new MedicationManagement();
            CreateMMCommand = new RelayCommand(CreateMM);
            SubstitutionTypes = new ObservableCollection<SubstitutionType>(Enum.GetValues(typeof(SubstitutionType)).Cast<SubstitutionType>().ToList());
            BackCommand = new RelayCommand(Back);
        }

        public MedicationManagementViewModel(MedicationManagement mm, bool showDetailMode)
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = false;
            _showDetailMode = showDetailMode;
            _currentMM = mm;
            CreateMMCommand = new RelayCommand(CreateMM);
            SubstitutionTypes = new ObservableCollection<SubstitutionType>(Enum.GetValues(typeof(SubstitutionType)).Cast<SubstitutionType>().ToList());
            BackCommand = new RelayCommand(Back);
        }

        public void CreateMM()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                WeakReferenceMessenger.Default.Send(new NewArchetypeAddedMessage(CurrentMM));
                CurrentMM = new MedicationManagement();
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void Back()
        {
            if (!IsEditMode)
            {
                if (_showDetailMode)
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("Prev"));
                }
                else
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("New"));
                }
            } else
            {
                WeakReferenceMessenger.Default.Send(new BackToNewVisitMessage(""));
            }
            
            CurrentMM = new MedicationManagement();
            IsEditMode = true;
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateText(_currentMM.MedicationItem, 128))
            {
                _ = sb.Append("Wrong 'Medication item' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMM.Amount, 32))
            {
                _ = sb.Append("Wrong 'Amount' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMM.ClinicalInd, 64))
            {
                _ = sb.Append("Wrong 'Clinical indication' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMM.SubstitutionReason, 256))
            {
                _ = sb.Append("Wrong 'Substitution reason' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMM.Route, 64))
            {
                _ = sb.Append("Wrong 'Route' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMM.BodySite, 64))
            {
                _ = sb.Append("Wrong 'Body site' format!\n");
            }
            if (_currentMM.PatientGuidance != null && _currentMM.PatientGuidance.Length > 0 && !ValidationUtils.ValidateText(_currentMM.PatientGuidance, 256))
            {
                _ = sb.Append("Wrong 'Patient guidance' format!\n");
            }
            if (_currentMM.Comment != null && _currentMM.Comment.Length > 0 && !ValidationUtils.ValidateText(_currentMM.Comment, 500))
            {
                _ = sb.Append("Wrong 'Comment' format!\n");
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
