﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class HealthEducationViewModel : ObservableObject
    {
        public ICommand CreateHECommand { get; }
        public ICommand BackCommand { get; }

        private HealthEducation _currentHE;
        public HealthEducation CurrentHE
        {
            get => _currentHE;
            set => SetProperty(ref _currentHE, value);
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => SetProperty(ref _isEditMode, value);
        }

        private bool _showDetailMode;

        public HealthEducationViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = true;
            _currentHE = new HealthEducation();
            CreateHECommand = new RelayCommand(CreateHE);
            BackCommand = new RelayCommand(Back);
        }

        public HealthEducationViewModel(HealthEducation he, bool showDetailMode)
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = false;
            _showDetailMode = showDetailMode;
            _currentHE = he;
            CreateHECommand = new RelayCommand(CreateHE);
            BackCommand = new RelayCommand(Back);
        }

        public void CreateHE()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                WeakReferenceMessenger.Default.Send(new NewArchetypeAddedMessage(CurrentHE));
                CurrentHE = new HealthEducation();
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void Back()
        {
            if (!IsEditMode)
            {
                if (_showDetailMode)
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("Prev"));
                }
                else
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("New"));
                }
            } else
            {
                WeakReferenceMessenger.Default.Send(new BackToNewVisitMessage(""));
            }
            
            CurrentHE = new HealthEducation();
            IsEditMode = true;
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateText(_currentHE.TopicName, 128))
            {
                _ = sb.Append("Wrong 'Topic name' format!\n");
            }
            if (_currentHE.Description != null && _currentHE.Description.Length > 0 && !ValidationUtils.ValidateText(_currentHE.Description, 300))
            {
                _ = sb.Append("Wrong 'Description' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentHE.ClinicalInd, 64))
            {
                _ = sb.Append("Wrong 'Clinical indication' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentHE.Method, 64))
            {
                _ = sb.Append("Wrong 'Method' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentHE.Outcome, 300))
            {
                _ = sb.Append("Wrong 'Outcome' format!\n");
            }
            if (_currentHE.Comment != null && _currentHE.Comment.Length > 0 && !ValidationUtils.ValidateText(_currentHE.Comment, 500))
            {
                _ = sb.Append("Wrong 'Comment' format!\n");
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
