﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class HomePageViewModel : ObservableObject, IRecipient<PatientAddedMessage>
    {
        private ObservableCollection<Patient> _patients;
        public ObservableCollection<Patient> Patients
        {
            get => _patients;
            set => SetProperty(ref _patients, value);
        }

        public ICommand SelectPatientCommand { get; }

        public HomePageViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);
            Patients = new ObservableCollection<Patient>();
            SelectPatientCommand = new RelayCommand<Patient>(SelectPatient);
            LoadPatients();
        }

        public void LoadPatients()
        {
            Patients = new ObservableCollection<Patient>(PatientService.GetAllPatients());
        }

        public void SelectPatient(Patient p)
        {
            WeakReferenceMessenger.Default.Send(new PatientSelectedMessage(p));
        }

        public void Receive(PatientAddedMessage message)
        {
            LoadPatients();
        }
    }
}
