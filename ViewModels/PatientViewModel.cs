﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Services;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class PatientViewModel : ObservableObject
    {
        public ICommand CreatePCommand { get; }
        public ICommand CancelCommand { get; }

        private Patient _currentP;
        public Patient CurrentP
        {
            get => _currentP;
            set => SetProperty(ref _currentP, value);
        }

        public PatientViewModel()
        {
            _currentP = new Patient();
            CreatePCommand = new RelayCommand(CreateP);
            CancelCommand = new RelayCommand(Cancel);
        }

        public void CreateP()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                PatientService.CreatePatient(CurrentP);

                WeakReferenceMessenger.Default.Send(new PatientAddedMessage(CurrentP));
                CurrentP = new Patient();
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void Cancel()
        {
            WeakReferenceMessenger.Default.Send(new PatientAddedMessage(null));
            CurrentP = new Patient();
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateText(_currentP.FirstName, 64))
            {
                _ = sb.Append("Wrong 'First name' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentP.LastName, 64))
            {
                _ = sb.Append("Wrong 'Last name' format!\n");
            }
            if (!ValidationUtils.ValidatePIN(_currentP.PersIdenNum))
            {
                _ = sb.Append("Wrong 'PersIdenNum' format!\n");
            }
            if (_currentP.Email != null && _currentP.Email.Length > 0 && !ValidationUtils.ValidateEmail(_currentP.Email))
            {
                _ = sb.Append("Wrong 'Email' format!\n");
            }
            if (_currentP.Phone != null && _currentP.Phone.Length > 0 && !ValidationUtils.ValidatePhone(_currentP.Phone))
            {
                _ = sb.Append("Wrong 'Phone' format!\n");
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
