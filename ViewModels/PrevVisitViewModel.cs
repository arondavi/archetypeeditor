﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class PrevVisitViewModel : ObservableObject, IRecipient<ShowPrevVisitMessage>
    {
        public ICommand BackCommand { get; }
        public ICommand ShowArchetypeCommand { get; }

        private Visit _visit;
        public Visit Visit
        {
            get => _visit;
            set => SetProperty(ref _visit, value);
        }

        private ObservableCollection<Archetype> _archetypes;
        public ObservableCollection<Archetype> Archetypes
        {
            get => _archetypes;
            set {
                SetProperty(ref _archetypes, value);
                ArchetypesEmpty = _archetypes == null || _archetypes.Count <= 0;
            }
        }

        private bool _archetypesEmpty;
        public bool ArchetypesEmpty
        {
            get => _archetypesEmpty;
            set => SetProperty(ref _archetypesEmpty, value);
        }


        public PrevVisitViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);
            BackCommand = new RelayCommand(Back);
            ShowArchetypeCommand = new RelayCommand<Archetype>(ShowArchetype);
            _archetypesEmpty = true;
        }

        public void ShowArchetype(Archetype arch)
        {
            WeakReferenceMessenger.Default.Send(new ShowArchetypeDetailMessage((arch, true)));
        }

        public void Receive(ShowPrevVisitMessage message)
        {
            Visit = VisitService.GetVisitByID(message.Value);
            Archetypes = new ObservableCollection<Archetype>(ArchetypeService.GetArchetypesByVisitID(message.Value));
        }

        public void Back()
        {
            WeakReferenceMessenger.Default.Send(new ClosePrevVisitMessage(""));
            Visit = null;
            Archetypes = null;
        }
    }
}
