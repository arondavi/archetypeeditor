﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class MedicationOrderViewModel : ObservableObject
    {
        public ICommand CreateMOCommand { get; }
        public ICommand BackCommand { get; }

        private MedicationOrder _currentMO;

        public MedicationOrder CurrentMO
        {
            get => _currentMO;
            set => SetProperty(ref _currentMO, value);
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => SetProperty(ref _isEditMode, value);
        }

        private bool _showDetailMode;

        public MedicationOrderViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = true;
            _currentMO = new MedicationOrder();
            CreateMOCommand = new RelayCommand(CreateMO);
            BackCommand = new RelayCommand(Back);
        }

        public MedicationOrderViewModel(MedicationOrder mo, bool showDetailMode)
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = false;
            _showDetailMode = showDetailMode;
            _currentMO = mo;
            CreateMOCommand = new RelayCommand(CreateMO);
            BackCommand = new RelayCommand(Back);
        }

        public void CreateMO()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                WeakReferenceMessenger.Default.Send(new NewArchetypeAddedMessage(CurrentMO));
                CurrentMO = new MedicationOrder();
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void Back()
        {
            if (!IsEditMode)
            {
                if (_showDetailMode)
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("Prev"));
                }
                else
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("New"));
                }
            } else
            {
                WeakReferenceMessenger.Default.Send(new BackToNewVisitMessage(""));
            }

            CurrentMO = new MedicationOrder();
            IsEditMode = true;
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateText(_currentMO.MedicationItem, 128))
            {
                _ = sb.Append("Wrong 'Medication item' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMO.Route, 256))
            {
                _ = sb.Append("Wrong 'Route' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentMO.BodySite, 64))
            {
                _ = sb.Append("Wrong 'Body site' format!\n");
            }
            if (_currentMO.Odd != null && _currentMO.Odd.Length > 0 && !ValidationUtils.ValidateText(_currentMO.Odd, 500))
            {
                _ = sb.Append("Wrong 'ODD' format!\n");
            }
            if (_currentMO.PatientInfo != null && _currentMO.PatientInfo.Length > 0 && !ValidationUtils.ValidateText(_currentMO.PatientInfo, 500))
            {
                _ = sb.Append("Wrong 'Patient info' format!\n");
            }
            if (_currentMO.Comment != null && _currentMO.Comment.Length > 0 && !ValidationUtils.ValidateText(_currentMO.Comment, 500))
            {
                _ = sb.Append("Wrong 'Comment' format!\n");
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
