﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class NewVisitViewModel : ObservableObject, IRecipient<NewArchetypeAddedMessage>, IRecipient<ExitProfileMessage>
    {
        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();

        public ICommand SwitchFramePageCommand { get; }
        public ICommand SubmitVisitCommand { get; }
        public ICommand ShowArchetypeCommand { get; }
        public ICommand RemoveArchetypeCommand { get; }

        private string _currentTime;
        public string CurrentTime
        {
            get => _currentTime;
            set => SetProperty(ref _currentTime, value);
        }

        private ObservableCollection<Archetype> _currentArchetypes;
        public ObservableCollection<Archetype> CurrentArchetypes
        {
            get => _currentArchetypes;
            set
            {
                SetProperty(ref _currentArchetypes, value);
                ArchetypesEmpty = _currentArchetypes == null || _currentArchetypes.Count <= 0;
            }
        }

        private bool _archetypesEmpty;
        public bool ArchetypesEmpty
        {
            get => _archetypesEmpty;
            set => SetProperty(ref _archetypesEmpty, value);
        }

        public NewVisitViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            CurrentArchetypes = new ObservableCollection<Archetype>();

            SwitchFramePageCommand = new RelayCommand<string>(SwitchFramePage);
            SubmitVisitCommand = new RelayCommand(Submit);
            ShowArchetypeCommand = new RelayCommand<Archetype>(ShowArchetype);
            RemoveArchetypeCommand = new RelayCommand<Archetype>(RemoveArchetype);
            _archetypesEmpty = true;

            Timer.Tick += new EventHandler(Timer_Click);
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
        }

        public void Submit()
        {
            if(CurrentArchetypes.Count > 0)
            {
                WeakReferenceMessenger.Default.Send(new NewVisitSubmitedMessage(CurrentArchetypes.ToList()));
                CurrentArchetypes.Clear();
                ArchetypesEmpty = _currentArchetypes == null || _currentArchetypes.Count <= 0;
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage("No examinations have been done!"));
            }
        }

        public void ShowArchetype(Archetype arch)
        {
            WeakReferenceMessenger.Default.Send(new ShowArchetypeDetailMessage((arch, false)));
        }

        public void RemoveArchetype(Archetype arch)
        {
            CurrentArchetypes.Remove(arch);
            ArchetypesEmpty = _currentArchetypes == null || _currentArchetypes.Count <= 0;
        }

        private void SwitchFramePage(string page)
        {
            WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage(page));
        }

        private void Timer_Click(object sender, EventArgs e)
        {
            CurrentTime = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
        }

        public void Receive(NewArchetypeAddedMessage message)
        {
            CurrentArchetypes.Add(message.Value);
            ArchetypesEmpty = _currentArchetypes == null || _currentArchetypes.Count <= 0;
        }

        public void Receive(ExitProfileMessage message)
        {
            CurrentArchetypes.Clear();
            ArchetypesEmpty = _currentArchetypes == null || _currentArchetypes.Count <= 0;
        }
    }
}
