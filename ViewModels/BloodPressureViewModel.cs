﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Utils;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System.Text;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class BloodPressureViewModel : ObservableObject
    {
        public ICommand CreateBPCommand { get; }
        public ICommand BackCommand { get; }

        private BloodPressure _currentBP;

        public BloodPressure CurrentBP
        {
            get => _currentBP;
            set => SetProperty(ref _currentBP, value);
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get => _isEditMode;
            set => SetProperty(ref _isEditMode, value);
        }

        private bool _showDetailMode;

        public BloodPressureViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = true;
            _currentBP = new BloodPressure();
            CreateBPCommand = new RelayCommand(CreateBP);
            BackCommand = new RelayCommand(Back);
        }

        public BloodPressureViewModel(BloodPressure bp, bool showDetailMode)
        {
            WeakReferenceMessenger.Default.RegisterAll(this);

            _isEditMode = false;
            _showDetailMode = showDetailMode;
            _currentBP = bp;
            CreateBPCommand = new RelayCommand(CreateBP);
            BackCommand = new RelayCommand(Back);
        }

        public void CreateBP()
        {
            var validation = ValidateInputs();
            if (validation.Item1)
            {
                WeakReferenceMessenger.Default.Send(new NewArchetypeAddedMessage(CurrentBP));
                CurrentBP = new BloodPressure();
            } else
            {
                WeakReferenceMessenger.Default.Send(new ShowErrorMessage(validation.Item2));
            }
        }

        public void Back()
        {
            if (!IsEditMode)
            {
                if (_showDetailMode)
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("Prev"));
                } else
                {
                    WeakReferenceMessenger.Default.Send(new SwitchFramePageMessage("New"));
                }
            } else
            {
                WeakReferenceMessenger.Default.Send(new BackToNewVisitMessage(""));
            }
            CurrentBP = new BloodPressure();
            IsEditMode = true;
        }

        private (bool, string) ValidateInputs()
        {
            StringBuilder sb = new StringBuilder("");
            if (!ValidationUtils.ValidateInt(_currentBP.Systolic,0,1000))
            {
                _ = sb.Append("Wrong 'Systolic' format!\n");
            }
            if (!ValidationUtils.ValidateInt(_currentBP.Diastolic, 0, 1000))
            {
                _ = sb.Append("Wrong 'Diastolic' format!\n");
            }
            if (!ValidationUtils.ValidateInt(_currentBP.MeanAP, 0, 1000))
            {
                _ = sb.Append("Wrong 'MeanAP' format!\n");
            }
            if (!ValidationUtils.ValidateInt(_currentBP.Pulse, 0, 1000))
            {
                _ = sb.Append("Wrong 'Pulse' format!\n");
            }
            if (!ValidationUtils.ValidateText(_currentBP.ClinicalInter, 200))
            {
                _ = sb.Append("Wrong 'Clinical interpretation' format!\n");
            }
            if (_currentBP.Comment != null && _currentBP.Comment.Length > 0 && !ValidationUtils.ValidateText(_currentBP.Comment, 500))
            {
                _ = sb.Append("Wrong 'Comment' format!\n");
            }

            return sb.Length > 0 ? (false, sb.ToString()) : (true, null);
        }
    }
}
