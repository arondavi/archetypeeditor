﻿using ArchetypeEditor.Messages;
using ArchetypeEditor.Models;
using ArchetypeEditor.Services;
using ArchetypeEditor.Views;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace ArchetypeEditor.ViewModels
{
    public class PatientProfileViewModel : ObservableObject, IRecipient<PatientSelectedMessage>,
        IRecipient<SwitchFramePageMessage>, IRecipient<NewArchetypeAddedMessage>, IRecipient<BackToNewVisitMessage>,
        IRecipient<NewVisitSubmitedMessage>, IRecipient<ClosePrevVisitMessage>, IRecipient<ShowArchetypeDetailMessage>
    {
        private readonly IDictionary<string, Page> pages;
        public ICommand ExitProfileCommand { get; }
        public ICommand ShowPrevVisitCommand { get; }

        private Page _profileFramePage;
        public Page ProfileFramePage
        {
            get => _profileFramePage;
            set => SetProperty(ref _profileFramePage, value);
        }

        private Patient _patient;
        public Patient Patient
        {
            get => _patient;
            set => SetProperty(ref _patient, value);
        }

        private ObservableCollection<Visit> _prevVisits;
        public ObservableCollection<Visit> PrevVisits
        {
            get => _prevVisits;
            set => SetProperty(ref _prevVisits, value);
        }

        public PatientProfileViewModel()
        {
            WeakReferenceMessenger.Default.RegisterAll(this);
            ExitProfileCommand = new RelayCommand(ExitProfile);
            ShowPrevVisitCommand = new RelayCommand<Visit>(ShowPrevVisit);

            pages = new Dictionary<string, Page>() {
                {"New", new NewVisitPage()},
                {"BP", new BloodPressurePage()},
                {"MO", new MedicationOrderPage()},
                {"HE", new HealthEducationPage()},
                {"HRA", new HealthRiskAssessmentPage()},
                {"MM", new MedicationManagementPage()},
                {"Prev", new PrevVisitPage()},
            };

            SwitchFramePage("New");
        }

        private void ExitProfile()
        {
            WeakReferenceMessenger.Default.Send(new ExitProfileMessage(Patient));
            SwitchFramePage("New");
            PrevVisits.Clear();
        }

        private void ShowPrevVisit(Visit visit)
        {
            WeakReferenceMessenger.Default.Send(new ShowPrevVisitMessage(visit.ID));
            SwitchFramePage("Prev");
        }

        public void SwitchFramePage(string key)
        {
            if (pages.TryGetValue(key, out Page page))
            {
                ProfileFramePage = page;
            }
        }

        private void LoadVisits()
        {
            PrevVisits = new ObservableCollection<Visit>(VisitService.GetPrevVisitsByPatientID(Patient.ID).OrderByDescending(v => v.Created));
        }

        public void Receive(PatientSelectedMessage message)
        {
            Patient = message.Value;
            LoadVisits();
        }

        public void Receive(SwitchFramePageMessage message)
        {
            SwitchFramePage(message.Value);
        }

        public void Receive(NewArchetypeAddedMessage message)
        {
            SwitchFramePage("New");
        }

        public void Receive(BackToNewVisitMessage message)
        {
            SwitchFramePage("New");
        }

        public void Receive(NewVisitSubmitedMessage message)
        {
            List<Archetype> archs = message.Value;

            Visit v = new Visit();
            v.Patient = Patient;
            v.Created = DateTime.Now;

            Visit newV = VisitService.CreateVisit(v);
            foreach (Archetype arch in archs)
            {
                if (arch is BloodPressure pressure)
                {
                    pressure.VisitID = newV.ID;
                    BloodPressureService.CreateBP(pressure);
                }

                if (arch is HealthEducation education)
                {
                    education.VisitID = newV.ID;
                    HealthEducationService.CreateHE(education);
                }

                if (arch is MedicationOrder order)
                {
                    order.VisitID = newV.ID;
                    MedicationOrderService.CreateMO(order);
                }

                if (arch is MedicationManagement management)
                {
                    management.VisitID = newV.ID;
                    MedicationManagementService.CreateMM(management);
                }

                if (arch is HealthRiskAssessment riskAssessment)
                {
                    riskAssessment.VisitID = newV.ID;
                    HealthRiskAssessmentService.CreateHRA(riskAssessment);
                }
            }

            LoadVisits();
        }

        public void Receive(ClosePrevVisitMessage message)
        {
            SwitchFramePage("New");
        }

        public void Receive(ShowArchetypeDetailMessage message)
        {
            if (message.Value.Item1 is BloodPressure)
            {
                pages["BP"].DataContext = new BloodPressureViewModel((BloodPressure)message.Value.Item1, message.Value.Item2);

                SwitchFramePage("BP");
            }

            if (message.Value.Item1 is HealthEducation)
            {
                pages["HE"].DataContext = new HealthEducationViewModel((HealthEducation)message.Value.Item1, message.Value.Item2);
                SwitchFramePage("HE");
            }

            if (message.Value.Item1 is HealthRiskAssessment)
            {
                pages["HRA"].DataContext = new HealthRiskAssessmentViewModel((HealthRiskAssessment)message.Value.Item1, message.Value.Item2);
                SwitchFramePage("HRA");
            }

            if (message.Value.Item1 is MedicationManagement)
            {
                pages["MM"].DataContext = new MedicationManagementViewModel((MedicationManagement)message.Value.Item1, message.Value.Item2);
                SwitchFramePage("MM");
            }

            if (message.Value.Item1 is MedicationOrder)
            {
                pages["MO"].DataContext = new MedicationOrderViewModel((MedicationOrder)message.Value.Item1, message.Value.Item2);
                SwitchFramePage("MO");
            }
        }
    }
}
