﻿namespace ArchetypeEditor.Models
{
    public class BloodPressure : Archetype
    {

        private int _systolic;
        public int Systolic
        {
            get { return _systolic; }
            set { _systolic = value; }
        }

        private int _diastolic;
        public int Diastolic
        {
            get { return _diastolic; }
            set { _diastolic = value; }
        }

        private int _meanAP;
        public int MeanAP
        {
            get { return _meanAP; }
            set { _meanAP = value; }
        }

        private int _pulse;
        public int Pulse
        {
            get { return _pulse; }
            set { _pulse = value; }
        }

        private string _clinicalInter;
        public string ClinicalInter
        {
            get { return _clinicalInter; }
            set { _clinicalInter = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _visitID;

        public int VisitID
        {
            get { return _visitID; }
            set { _visitID = value; }
        }

        public BloodPressure()
        {
            ConceptName = "Blood pressure";
            ConceptDescription = "The local measurement of arterial blood pressure which is a surrogate for arterial pressure in the systemic circulation.";
        }

        public BloodPressure(int systolic, int diastolic, int meanAP, int pulse, string clinicalInter, string comment)
        {
            ConceptName = "Blood pressure";
            ConceptDescription = "The local measurement of arterial blood pressure which is a surrogate for arterial pressure in the systemic circulation.";

            _systolic = systolic;
            _diastolic = diastolic;
            _meanAP = meanAP;
            _pulse = pulse;
            _clinicalInter = clinicalInter;
            _comment = comment;
        }
    }
}
