﻿using System.Collections.Generic;

namespace ArchetypeEditor.Models
{
    public class HealthRiskAssessment : Archetype
    {
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _healthRisk;
        public string HealthRisk
        {
            get { return _healthRisk; }
            set { _healthRisk = value; }
        }

        private List<RiskFactor> _riskFactors;
        public List<RiskFactor> RiskFactors
        {
            get { return _riskFactors; }
            set { _riskFactors = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _visitID;

        public int VisitID
        {
            get { return _visitID; }
            set { _visitID = value; }
        }

        public HealthRiskAssessment()
        {
            ConceptName = "Health risk assessment";
            ConceptDescription = "Assessment of the potential and likelihood of future adverse health effects as determined by identified risk factors.";
            RiskFactors = new List<RiskFactor>();
        }

        public HealthRiskAssessment(int id, string healthRisk, string comment)
        {
            ConceptName = "Health risk assessment";
            ConceptDescription = "Assessment of the potential and likelihood of future adverse health effects as determined by identified risk factors.";
            RiskFactors = new List<RiskFactor>();
            _id = id;
            _healthRisk = healthRisk;
            _comment = comment;
        }
    }
}
