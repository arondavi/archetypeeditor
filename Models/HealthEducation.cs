﻿namespace ArchetypeEditor.Models
{
    public class HealthEducation : Archetype
    {
        private string _topicName;
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _clinicalInd;
        public string ClinicalInd
        {
            get { return _clinicalInd; }
            set { _clinicalInd = value; }
        }

        private string _method;
        public string Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private string _outcome;
        public string Outcome
        {
            get { return _outcome; }
            set { _outcome = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _visitID;

        public int VisitID
        {
            get { return _visitID; }
            set { _visitID = value; }
        }

        public HealthEducation()
        {
            ConceptName = "Health education";
            ConceptDescription = "Communication to improve health literacy and life skills.";
        }

        public HealthEducation(string topicName, string description, string clinicalInd, string method, string outcome, string comment)
        {
            ConceptName = "Health education";
            ConceptDescription = "Communication to improve health literacy and life skills.";

            _topicName = topicName;
            _description = description;
            _clinicalInd = clinicalInd;
            _method = method;
            _outcome = outcome;
            _comment = comment;
        }
    }
}
