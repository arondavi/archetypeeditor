﻿namespace ArchetypeEditor.Models
{
    public class MedicationOrder : Archetype
    {
        private string _medicationItem;

        public string MedicationItem
        {
            get { return _medicationItem; }
            set { _medicationItem = value; }
        }

        private string _route;

        public string Route
        {
            get { return _route; }
            set { _route = value; }
        }

        private string _bodySite;

        public string BodySite
        {
            get { return _bodySite; }
            set { _bodySite = value; }
        }

        private string _odd;

        public string Odd
        {
            get { return _odd; }
            set { _odd = value; }
        }

        private string _patienInfo;

        public string PatientInfo
        {
            get { return _patienInfo; }
            set { _patienInfo = value; }
        }

        private string _comment;

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _visitID;

        public int VisitID
        {
            get { return _visitID; }
            set { _visitID = value; }
        }

        public MedicationOrder()
        {
            ConceptName = "Medication order";
            ConceptDescription = "An order for a medication, vaccine, nutritional product or other therapeutic item for an identified individual.";
        }

        public MedicationOrder(string medItem, string route, string bodySite, string odd, string patInfo, string comment)
        {
            ConceptName = "Medication order";
            ConceptDescription = "An order for a medication, vaccine, nutritional product or other therapeutic item for an identified individual.";
            _medicationItem = medItem;
            _route = route;
            _bodySite = bodySite;
            _odd = odd;
            _patienInfo = patInfo;
            _comment = comment;
        }
    }
}
