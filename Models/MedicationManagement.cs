﻿using ArchetypeEditor.Models.Enums;

namespace ArchetypeEditor.Models
{
    public class MedicationManagement : Archetype
    {
        private string _medicationItem;
        public string MedicationItem
        {
            get { return _medicationItem; }
            set { _medicationItem = value; }
        }

        private string _amount;
        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _clinicalInd;
        public string ClinicalInd
        {
            get { return _clinicalInd; }
            set { _clinicalInd = value; }
        }

        private SubstitutionType _substitution;
        public SubstitutionType Substitution
        {
            get { return _substitution; }
            set { _substitution = value; }
        }

        private string _substitutionReason;
        public string SubstitutionReason
        {
            get { return _substitutionReason; }
            set { _substitutionReason = value; }
        }

        private string _route;

        public string Route
        {
            get { return _route; }
            set { _route = value; }
        }

        private string _bodySite;
        public string BodySite
        {
            get { return _bodySite; }
            set { _bodySite = value; }
        }

        private string _patientGuidance;

        public string PatientGuidance
        {
            get { return _patientGuidance; }
            set { _patientGuidance = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _visitID;

        public int VisitID
        {
            get { return _visitID; }
            set { _visitID = value; }
        }

        public MedicationManagement()
        {
            ConceptName = "Medication management";
            ConceptDescription = "Any activity related to the planning, scheduling, prescription management, dispensing, administration, cessation and other use of a medication, vaccine, nutritional product or other therapeutic item.";
        }

        public MedicationManagement(string medItem, string amount, string clinicalInd, int substitution, string subReason, string rout, string bodySite, string patientGui, string comment)
        {
            ConceptName = "Medication management";
            ConceptDescription = "Any activity related to the planning, scheduling, prescription management, dispensing, administration, cessation and other use of a medication, vaccine, nutritional product or other therapeutic item.";
            _medicationItem = medItem;
            _amount = amount;
            _clinicalInd = clinicalInd;
            _substitution = (SubstitutionType)substitution;
            _substitutionReason = subReason;
            _route = rout;
            _bodySite = bodySite;
            _patientGuidance = patientGui;
            _comment = comment;
        }
    }
}
