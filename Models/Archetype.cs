﻿namespace ArchetypeEditor.Models
{
    public class Archetype
    {
        private string _conceptName;
        public string ConceptName
        {
            get { return _conceptName; }
            set { _conceptName = value; }
        }
        private string _conceptDescription;
        public string ConceptDescription
        {
            get { return _conceptDescription; }
            set { _conceptDescription = value; }
        }
    }
}
