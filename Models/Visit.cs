﻿using System;

namespace ArchetypeEditor.Models
{
    public class Visit
    {
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _created;

        public DateTime Created
        {
            get { return _created; }
            set { _created = value; }
        }

        private Patient _patient;

        public Patient Patient
        {
            get { return _patient; }
            set { _patient = value; }
        }

        public Visit()
        {

        }

        public Visit(int id, DateTime created)
        {
            _id = id;
            _created = created;
        }
    }
}
