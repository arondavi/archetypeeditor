﻿using ArchetypeEditor.Models.Enums;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace ArchetypeEditor.Models
{
    public class RiskFactor
    {
        private string _riskFactorN;
        public string RiskFactorN
        {
            get { return _riskFactorN; }
            set { _riskFactorN = value; }
        }

        private PresenceRiskFactorType _presence;
        public PresenceRiskFactorType Presence
        {
            get { return _presence; }
            set { _presence = value; }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public ObservableCollection<PresenceRiskFactorType> PresenceTypes { get; }

        public RiskFactor()
        {
            PresenceTypes = new ObservableCollection<PresenceRiskFactorType>(Enum.GetValues(typeof(PresenceRiskFactorType)).Cast<PresenceRiskFactorType>().ToList());
        }

        public RiskFactor(string rF, int presence, string description, string comment)
        {
            PresenceTypes = new ObservableCollection<PresenceRiskFactorType>(Enum.GetValues(typeof(PresenceRiskFactorType)).Cast<PresenceRiskFactorType>().ToList());
            _riskFactorN = rF;
            _presence = (PresenceRiskFactorType)presence;
            _description = description;
            _comment = comment;
        }
    }
}
