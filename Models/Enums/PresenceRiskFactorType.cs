﻿namespace ArchetypeEditor.Models.Enums
{
    public enum PresenceRiskFactorType
    {
        Present, Indeterminate, Absent
    }
}
