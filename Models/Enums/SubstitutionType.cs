﻿namespace ArchetypeEditor.Models.Enums
{
    public enum SubstitutionType
    {
        SubstitutionPerformed, SubstitutionNotPerformed
    }
}
