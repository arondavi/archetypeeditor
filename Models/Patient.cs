﻿namespace ArchetypeEditor.Models
{
    public class Patient
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string FullName
        {
            get { return _firstName + " " + _lastName; }
        }

        private string _persIdenNum;

        public string PersIdenNum
        {
            get { return _persIdenNum; }
            set { _persIdenNum = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _phone;

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public Patient(int id, string fn, string ln, string pin, string email, string phone)
        {
            _id = id;
            _firstName = fn;
            _lastName = ln;
            _persIdenNum = pin;
            _email = email;
            _phone = phone;
        }

        public Patient()
        {

        }
    }
}
